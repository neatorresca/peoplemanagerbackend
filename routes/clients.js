var express = require('express');
var router = express.Router();
const Client = require('../lib/model/client');

router
// POST client creation 
.post('/',(req,res,next)=>{
	if(!req.body){
		res
			.status(403)
			.json({error:true, message:'Params empty'})
	}else{
		let _client = req.body

		new Client({
			id: _client.id,
			name: _client.name,
			phone: _client.phone
		})
		.save((err,client)=>{
			if(err){
				res
					.status(403)
					.json({error:true, message:'Failed to create user'})
			}else{
				res
					.status(201)
					.json({client:client})
			}
		})
	}
})
// GET clients listing. 
.get('/', function(req, res, next) {
	Client.find({},(err,clients)=>{
		//res
			//.status(200)
			//.json({clients:{'id':1,'name':'neiro','phone':123}})
		if(err){
			res
				.status(403)
				.json({error:true, message:'Params empty'})
		}else{
			res
				.status(200)
				.json({clients: clients})
		}
	})
})

.get('/:id',function(req,res,next){
	if(!req.params && !req.body){
		res
			.status(403)
			.json({error:true, message:"Params empty"})
	}else{
		let _id = req.params.id
		Client.findOne({id: _id},(err,client)=>{
			if(err){
				res
					.status(403)
					.json({error:true, message:"Id not found"})
			}else if(client){
				res
					.status(200)
					.json({client:client})
			}
		})
	}
})
.put('/:id',(req,res,next)=>{
	if(!req.params || !req.body){
		res
			.status(403)
			.json({error:true, contenido:{"reqParams":req.params,"reqBody":req.body}})
	}else{
		let _id = parseInt(req.params.id)
		let _newClient = req.body

		Client.update({"id":_id},{
			"name":_newClient.name,
			"phone":_newClient.phone
		},(err)=>{
			if(err){
				res
					.status(403)
					.json({error:true, message:'Params empty'})
			}else{
				res
					.status(200)
					.json({client: _newClient})
			}
		})
	}
})

.delete('/:id',function(req, res, next){
	if(!req.params.id && !req.body){
		res
			.status(403)
			.json({error:true, message: 'Params empty'})
	}

	let id = parseInt(req.params.id)
	Client.remove({id: id},(err,done)=>{
		if(err){
			res.status(403)
				.json({error:true, message:'Params empty'})
		}else{
			res
				.status(200)
				.json({})
		}
	})
})
module.exports = router;