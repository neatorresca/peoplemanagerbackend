"use strict"
const mongoose = require('mongoose')
const ClientSchema = new mongoose.Schema({
	id: { type:Number, required:true, index: {unique:true}},
	name: { type:String, required:true},
	phone: { type:Number, required:true}
})

module.exports = mongoose.model('Client', ClientSchema)