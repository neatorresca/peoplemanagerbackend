GET: /clients 200
Listar los clientes

POST: /clients 201
Registrar nuevo cliente

GET: /clients/:id 200
Obtener un cliente

PUT: /clients/:id 200
Modificar un cliente

DELETE: /clients/:id 400
Eliminar un cliente